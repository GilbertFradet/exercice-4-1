package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.DirectAccessBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.MultiBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class DABadgeWalletJsonDAOTest {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File JSONwalletDatabase = new File(RESOURCES_PATH + "test.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException {
        if (JSONwalletDatabase.exists()) {
            JSONwalletDatabase.delete();
            JSONwalletDatabase.createNewFile();
        }
    }


    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabase() throws IOException, ParseException {

        try {

            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "test.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2022-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA034", begin, end, null, image2);
            dao.addBadge(badge2);

            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);

            // 4ème Badge
            File image4 = new File(RESOURCES_PATH + "petite_image");
            DigitalBadge badge4 = new DigitalBadge("NFA039", begin, end, null, image3);
            dao.addBadge(badge4);


            try (BufferedReader reader = new BufferedReader(new FileReader(JSONwalletDatabase))) {

                ImageSerializer serializer = new ImageSerializerBase64Impl();

                String serializedImage = reader.readLine();
                LOG.info("1ère ligne:\n{}", serializedImage);
                String encodedImage = (String) serializer.serialize(image);
                serializedImage = serializedImage.replaceAll("\n", "").replaceAll("\r", "");
                String[] data = serializedImage.split("\"");

                String serializedImage2 = reader.readLine();
                LOG.info("2ème ligne:\n{}", serializedImage2);
                String encodedImage2 = (String) serializer.serialize(image2);
                serializedImage2 = serializedImage2.replaceAll("\n", "").replaceAll("\r", "");
                String[] data2 = serializedImage2.split("\"");

                String serializedImage3 = reader.readLine();
                LOG.info("3ème ligne:\n{}", serializedImage3);
                String encodedImage3 = (String) serializer.serialize(image3);
                serializedImage3 = serializedImage3.replaceAll("\n", "").replaceAll("\r", "");
                String[] data3 = serializedImage3.split("\"");

                // Assertions

                assertEquals(1, Integer.parseInt(data[6].split(",")[0].split(":")[1]));
                assertEquals(0, Integer.parseInt(data[8].split(":")[1].split(",")[0]));
                assertEquals(Files.size(image.toPath()), Long.parseLong(data[10].split(":")[1].split("}")[0]));
                assertEquals(encodedImage, data[21]);

                assertEquals(2, Integer.parseInt(data2[6].split(",")[0].split(":")[1]));
                assertEquals(895, Integer.parseInt(data2[8].split(":")[1].split(",")[0]));
                assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[10].split(":")[1].split("}")[0]));
                assertEquals(encodedImage2, data2[21]);

               assertEquals(3, Integer.parseInt(data3[6].split(",")[0].split(":")[1]));
                assertEquals(2255, Integer.parseInt(data3[8].split(":")[1].split(",")[0]));
                assertEquals(Files.size(image3.toPath()), Long.parseLong(data3[10].split(":")[1].split("}")[0]));
                assertEquals(encodedImage3, data3[21]);
            }

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

}