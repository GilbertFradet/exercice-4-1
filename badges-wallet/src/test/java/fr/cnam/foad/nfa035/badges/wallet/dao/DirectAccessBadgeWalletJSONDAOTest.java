package fr.cnam.foad.nfa035.badges.wallet.dao;

import fr.cnam.foad.nfa035.badges.fileutils.simpleaccess.ImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.simpleaccess.ImageSerializerBase64Impl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.DirectAccessBadgeWalletJSONDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.JSONBadgeWalletDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.impl.json.MultiBadgeWalletJSONDAOImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test unitaire du DAO d'accès au Wallet de Badges digitaux
 */
public class DirectAccessBadgeWalletJSONDAOTest {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletDAOTest.class);

    private static final String RESOURCES_PATH = "src/test/resources/";
    private static final File JSONwalletDatabase = new File(RESOURCES_PATH + "test.json");

    /**
     * Initialisation avant chaque méthode de test
     *
     * @throws IOException
     */
    @BeforeEach
    public void init() throws IOException, ParseException {
        if (JSONwalletDatabase.exists()) {
            JSONwalletDatabase.delete();
            JSONwalletDatabase.createNewFile();
        }
        DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "test.json");

        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date begin = format.parse("2021-09-27");
        Date end = format.parse("2022-01-03");

        // 1er Badge
        File image = new File(RESOURCES_PATH + "petite_image.png");
        DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
        dao.addBadge(badge1);

        // 2ème Badge
        File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
        DigitalBadge badge2 = new DigitalBadge("NFA034", begin, end, null, image2);
        dao.addBadge(badge2);

        // 3ème Badge
        File image3 = new File(RESOURCES_PATH + "superman.jpg");
        DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
        dao.addBadge(badge3);
    }

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
   /* @Test
    public void testAddBadgeOnDatabaseException() {

        try {

            DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletDAOImpl(RESOURCES_PATH + "wallet.csv");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");

            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge);

            assertThrows(IOException.class, () -> {
                dao.addBadge(badge);
            });

        } catch (Exception e) {
            LOG.error("Test en échec ! ",e);
            fail();
        }
    }*/

    /**
     * Teste l'ajout d'un badge sur Base multi-badge
     */
    @Test
    public void testAddBadgeOnDatabase() {

        try {
            if (JSONwalletDatabase.exists()) {
                JSONwalletDatabase.delete();
                JSONwalletDatabase.createNewFile();
            }
            DirectAccessBadgeWalletDAO dao = new JSONBadgeWalletDAOImpl(RESOURCES_PATH + "test.json");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2022-01-03");

            // 1er Badge
            File image = new File(RESOURCES_PATH + "petite_image.png");
            DigitalBadge badge1 = new DigitalBadge("NFA033", begin, end, null, image);
            dao.addBadge(badge1);

            // 2ème Badge
            File image2 = new File(RESOURCES_PATH + "petite_image_2.png");
            DigitalBadge badge2 = new DigitalBadge("NFA034", begin, end, null, image2);
            dao.addBadge(badge2);

            // 3ème Badge
            File image3 = new File(RESOURCES_PATH + "superman.jpg");
            DigitalBadge badge3 = new DigitalBadge("NFA035", begin, end, null, image3);
            dao.addBadge(badge3);



            try (BufferedReader reader = new BufferedReader(new FileReader(JSONwalletDatabase))) {

                ImageSerializer serializer = new ImageSerializerBase64Impl();

                String serializedImage = reader.readLine();
                LOG.info("1ère ligne:\n{}", serializedImage);
                String encodedImage = (String) serializer.serialize(image);
                serializedImage = serializedImage.replaceAll("\n", "").replaceAll("\r", "");
                String[] data = serializedImage.split("\"");

                String serializedImage2 = reader.readLine();
                LOG.info("2ème ligne:\n{}", serializedImage2);
                String encodedImage2 = (String) serializer.serialize(image2);
                serializedImage2 = serializedImage2.replaceAll("\n", "").replaceAll("\r", "");
                String[] data2 = serializedImage2.split("\"");

                String serializedImage3 = reader.readLine();
                LOG.info("3ème ligne:\n{}", serializedImage3);
                String encodedImage3 = (String) serializer.serialize(image3);
                serializedImage3 = serializedImage3.replaceAll("\n", "").replaceAll("\r", "");
                String[] data3 = serializedImage3.split("\"");

                // Assertions

                assertEquals(1, Integer.parseInt(data[6].split(",")[0].split(":")[1]));
                assertEquals(0, Integer.parseInt(data[8].split(":")[1].split(",")[0]));
                assertEquals(Files.size(image.toPath()), Long.parseLong(data[10].split(":")[1].split("}")[0]));
                assertEquals(encodedImage, data[21]);

                assertEquals(2, Integer.parseInt(data2[6].split(",")[0].split(":")[1]));
                assertEquals(895, Integer.parseInt(data2[8].split(":")[1].split(",")[0]));
                assertEquals(Files.size(image2.toPath()), Long.parseLong(data2[10].split(":")[1].split("}")[0]));
                assertEquals(encodedImage2, data2[21]);

                assertEquals(3, Integer.parseInt(data3[6].split(",")[0].split(":")[1]));
                assertEquals(2255, Integer.parseInt(data3[8].split(":")[1].split(",")[0]));
                assertEquals(Files.size(image3.toPath()), Long.parseLong(data3[10].split(":")[1].split("}")[0]));
                assertEquals(encodedImage3, data3[21]);
            }

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }

    /**
     * Test la récupération séquentielle des badges du wallet
     */
    @Test
    public void testGetBadgeFromDatabase() throws ParseException {
        try {
            BadgeWalletDAO dao = new MultiBadgeWalletJSONDAOImpl(RESOURCES_PATH + "test.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");


            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            dao.getBadge(fileBadgeStream1);
            System.out.println("one");
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            dao.getBadge(fileBadgeStream2);
            System.out.println("two");
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            dao.getBadge(fileBadgeStream3);
            System.out.println("three");

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }


    /**
     * Teste la récupération des Métadonnées
     */

    @Test
    void testGetMetadata() throws IOException {
        DirectAccessBadgeWalletDAO dao = new DirectAccessBadgeWalletJSONDAOImpl(RESOURCES_PATH + "test.json");
        Set<DigitalBadge> metaSet = dao.getWalletMetadata();
        LOG.info("Et voici les Métadonnées: {}", dao.getWalletMetadata());

        assertEquals(3, metaSet.size());
        Iterator<DigitalBadge> it = new TreeSet(metaSet).iterator();
        assertEquals(new DigitalBadgeMetadata(1, 0, 557), it.next().getMetadata());
        assertEquals(new DigitalBadgeMetadata(2, 895, 906), it.next().getMetadata());
        assertEquals(new DigitalBadgeMetadata(3, 2255, 35664), it.next().getMetadata());
    }


/**
 * Test la récupération séquentielle des badges du wallet
 */

    @Test
    public void testGetBadgeFromDatabaseByMetadata(){
        try {
            BadgeWalletDAO dao = new DirectAccessBadgeWalletJSONDAOImpl(RESOURCES_PATH + "test.json");

            File originImage1 = new File(RESOURCES_PATH + "petite_image.png");
            File originImage2 = new File(RESOURCES_PATH + "petite_image_2.png");
            File originImage3 = new File(RESOURCES_PATH + "superman.jpg");
            File extractedImage1 = new File(RESOURCES_PATH + "petite_image_ext.png");
            File extractedImage2 = new File(RESOURCES_PATH + "petite_image_2_ext.png");
            File extractedImage3 = new File(RESOURCES_PATH + "superman_ext.jpg");

            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date begin = format.parse("2021-09-27");
            Date end = format.parse("2021-01-03");
            DigitalBadge expectedBadge1 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(1, 0,557), null);
            DigitalBadge expectedBadge2 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(2, 895,906), null);
            DigitalBadge expectedBadge3 = new DigitalBadge("NFA035", begin, end, new DigitalBadgeMetadata(3, 2255,35664), null);
            OutputStream fileBadgeStream1 = new FileOutputStream(extractedImage1);
            ((DirectAccessBadgeWalletJSONDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream1, expectedBadge1);
            OutputStream fileBadgeStream2 = new FileOutputStream(extractedImage2);
            ((DirectAccessBadgeWalletJSONDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream2, expectedBadge2);
            OutputStream fileBadgeStream3 = new FileOutputStream(extractedImage3);
            ((DirectAccessBadgeWalletJSONDAOImpl) dao).getBadgeFromMetadata(fileBadgeStream3, expectedBadge3);

            assertArrayEquals(Files.readAllBytes(originImage1.toPath()), Files.readAllBytes(extractedImage1.toPath()));
            LOG.info("Badge 1 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage2.toPath()), Files.readAllBytes(extractedImage2.toPath()));
            LOG.info("Badge 2 récupéré avec succès");
            assertArrayEquals(Files.readAllBytes(originImage3.toPath()), Files.readAllBytes(extractedImage3.toPath()));
            LOG.info("Badge 3 récupéré avec succès");

        } catch (Exception e) {
            LOG.error("Test en échec ! ", e);
            fail();
        }
    }


}
