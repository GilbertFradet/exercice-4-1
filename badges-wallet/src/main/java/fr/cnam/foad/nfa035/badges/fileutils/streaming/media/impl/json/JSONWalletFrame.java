package fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;

/**
 * @author tvonstebut
 * <p>
 * Implémentation d'ImageFrame pour un Fichier JSON comme canal
 * Permet notamment de gérer l'amorce du JSON afin de le rendre exploitable par accès direct (RandomAccessFile)
 */
public class JSONWalletFrame extends WalletFrame implements WalletFrameMedia, AutoCloseable {

    private static final Logger LOG = LogManager.getLogger(JSONWalletFrame.class);

    private BufferedReader encodedImageReader;
    private FileOutputStream encodedImageOutput = null;
    private long numberOfLines;

    /**
     * Constructeur élémentaire
     *
     * @param walletDatabase
     */
    public JSONWalletFrame(RandomAccessFile walletDatabase) {
        super(walletDatabase);
    }

    /**
     * {@inheritDoc}
     * <p>
     * Permet l'initialisation du fichier JSON (amorce) si celui-ci est vierge,
     * sinon récupère l'amorce et l'utilise pour initialiser les méta-données
     * globales du média (nombre de lignes...)
     *
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {

        if (encodedImageOutput == null) {

            RandomAccessFile file = getChannel();
            this.encodedImageOutput = new FileOutputStream(file.getFD());
            long fileLength = file.length();

            if (fileLength == 0) {
                // Ecriture de l'amorce
                Writer writer = new PrintWriter(encodedImageOutput, true, StandardCharsets.UTF_8);
                String s1 = MessageFormat.format("{0,number,#};{1,number,#};", 1, file.getFilePointer());
                ((PrintWriter) writer).printf("[[{\"badge\":{\"metadata\":{\"badgeId\":%1$d,\"walletPosition\":%2$d,\"imageSize\":-1},\"serial\":null,\"begin\":null,\"end\":null}},{\"payload\":null}]]", 1, file.getFilePointer());
                System.out.println(file.getFilePointer());
                writer.flush();
            } else {
                BufferedReader br = getEncodedImageReader(false);
                String lastLine = MetadataDeserializerDatabaseImpl.readLastLine(file);
                LOG.debug("Dernière ligne du fichier : {}", lastLine);
                String[] data = lastLine.split(":");
                data= data[3].split(",");
                this.numberOfLines = Long.parseLong(data[0]) - 1;
                file.seek(fileLength);
            }

        }
        return encodedImageOutput;
    }

    /**
     * {@inheritDoc}
     *
     * @return InputStream
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new BufferedInputStream(new FileInputStream(getChannel().getFD()));
    }


    /**
     * {@inheritDoc}
     */
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (encodedImageReader == null || !resume) {
            this.encodedImageReader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel().getFD()), StandardCharsets.UTF_8));
        }
        return this.encodedImageReader;
    }

    /**
     * {@inheritDoc}
     *
     * @return
     */
    @Override
    public long getNumberOfLines() {
        return numberOfLines;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void incrementLines() {
        this.numberOfLines++;
    }
}
