package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.proxy.Base64OutputStreamProxy;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.commons.codec.binary.Base64OutputStream;

import javax.sound.midi.Soundbank;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Set;

/**
 * Implémentation Base64 de sérialiseur d'image, basée sur des flux.
 * TODO
 */
public class JSONWalletSerializerDAImpl
        extends AbstractStreamingImageSerializer<DigitalBadge, JSONWalletFrame> {

    Set<DigitalBadge> metas;

    public JSONWalletSerializerDAImpl(Set<DigitalBadge> metas) {
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param source
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public InputStream getSourceInputStream(DigitalBadge source) throws IOException {
        return new FileInputStream(source.getBadge());
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public OutputStream getSerializingStream(JSONWalletFrame media) throws IOException {
        return new Base64OutputStreamProxy(new Base64OutputStream(media.getEncodedImageOutput(), true, 0, null));
    }


    @Override
    public final void serialize(DigitalBadge source, JSONWalletFrame media) throws IOException {
        RandomAccessFile random = media.getChannel();
        if (metas.contains(source)) {
            throw new IOException("Badge déjà présent dans le Wallet");
        }
        long size = Files.size(source.getBadge().toPath());
        try (OutputStream os = media.getEncodedImageOutput()) {
            long numberOfLines = media.getNumberOfLines();
            //long newPos = 0;
            long newPos = Long.parseLong(MetadataDeserializerJSONImpl.readLastLine(random).split(":")[4].split(",")[0]);
            random.seek(random.length() - 74);//Pour supprimer les partie nulles de l'amorce
            DigitalBadgeMetadata meta = new DigitalBadgeMetadata((int) numberOfLines + 1, newPos, size);
            source.setMetadata(meta);
            PrintWriter writer = new PrintWriter(os, true, StandardCharsets.UTF_8);
            //DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            // writer.printf("\"imageSize\":%1$d,\"serial\":%2$s,\"begin\":%3$s,\"end\":%4$s}},{\"payload\":", size, source.getSerial(), format.format(source.getBegin()), format.format(source.getEnd()));
            writer.printf("\"imageSize\":%1$d},\"serial\":\"%2$s\",\"begin\":%3$d,\"end\":%4$d}},{\"payload\":\"", size, source.getSerial(), source.getBegin().getTime(), source.getEnd().getTime());
            try (OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                eos.flush();
            }
            writer.printf("\"}],\n");//fin de ligne
            // amorce ligne suivante
            writer.printf("[{\"badge\":{\"metadata\":{\"badgeId\":%1$d,\"walletPosition\":%2$d,\"imageSize\":-1},\"serial\":null,\"begin\":null,\"end\":null}},{\"payload\":null}]]", numberOfLines + 2, random.getFilePointer());
        }
        media.incrementLines();
    }

}
