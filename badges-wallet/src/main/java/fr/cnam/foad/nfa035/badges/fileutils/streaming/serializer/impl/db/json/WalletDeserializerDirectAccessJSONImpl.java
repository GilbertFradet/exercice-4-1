package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializer;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class WalletDeserializerDirectAccessJSONImpl implements DirectAccessDatabaseDeserializer {

    private static final Logger LOG = LogManager.getLogger(WalletDeserializerDirectAccessJSONImpl.class);

    private OutputStream sourceOutputStream;
    Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     *
     * @param sourceOutputStream
     * @param metas              les métadonnées du wallet, si besoin
     */
    public WalletDeserializerDirectAccessJSONImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) {
        this.setSourceOutputStream(sourceOutputStream);
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);
        String str = media.getEncodedImageReader(false).readLine();
        String badge = str.split("payload\":\"")[1].split("\"")[0];
        // System.out.println("badge: "+badge);
        String serial=str.split("serial\":")[1].split(",")[0];
        //System.out.println("serial: "+serial);
        String begin =str.split("begin\":")[1].split(",")[0];
       //System.out.println("begin: "+begin);
        String end =str.split("end\":")[1].split("\\}")[0];
        System.out.println("end :"+end);
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(badge).transferTo(os);
        }
        targetBadge.setSerial(serial);
        targetBadge.setBegin(new Date(Long.parseLong(begin)));
        targetBadge.setEnd(new Date(Long.parseLong(end)));

    }

    /**
     * {@inheritDoc}
     *
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


    /**
     * {@inheritDoc}
     * <p>
     * Inutile => Non pris encharge par cet Objet
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }

    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }
}
