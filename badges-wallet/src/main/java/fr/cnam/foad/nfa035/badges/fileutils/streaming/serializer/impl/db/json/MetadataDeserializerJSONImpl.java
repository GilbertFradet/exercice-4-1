package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;


import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.BadgeDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.MetadataDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.MetadataDeserializerDatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadgeMetadata;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.fasterxml.jackson.core.JsonFactory;

public class MetadataDeserializerJSONImpl extends MetadataDeserializerDatabaseImpl implements MetadataDeserializer {
    private static final Logger LOG = LogManager.getLogger(MetadataDeserializerJSONImpl.class);

    @Override
    public Set<DigitalBadge> deserialize(WalletFrameMedia media) throws IOException {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        BufferedReader br = media.getEncodedImageReader(false);
        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET, false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);
        return br.lines()
                .map(
                        l -> {
                            try {
                                //String badge = l.split(",\\{\"payload")[0].split(".*badge\":")[1];
                                String badge1 = l.split(",\\{\"payload")[0].split("\\[\\{\"badge\":")[1].split(",\"serial\":")[0];//",\\{\"payload")[0]
                                String badge2 = l.split("\\},")[1];
                                String badge = badge1 +",\"badge\":null,"+ badge2;
                                // String badge = l.split("\\],")[0].split("\\[")[1];
                                /*
                                {
  "metadata" : {
    "badgeId" : 1,
    "walletPosition" : 0,
    "imageSize" : 557
  },
  "badge" : "/Users/o001389u/Desktop/CNAM3/NFA035/badges-wallet/bloc4/exercice-1/badges-wallet/src/test/resources/petite_image.png",
  "serial" : "NFA033",
  "begin" : 1632693600000,
  "end" : 1641164400000
 }
                                 */

                                System.out.println("badge :" + badge);
                                System.out.println("badge1 :" + badge1);
                                System.out.println("badge2 :" + badge2);
                                DigitalBadge digitalBadge = objectMapper.readValue(badge, DigitalBadge.class);
                                return digitalBadge.getMetadata().getImageSize() == -1 ? null : digitalBadge;
                            } catch (IOException ioException) {
                                LOG.error("Problème de parsage JSON, on considère l'enregistrement Nul", ioException);
                            }
                            return null;
                        }
                ).filter(x -> x != null).collect(Collectors.toSet());
    }


    /**
     * modifié
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static String readLastLine(RandomAccessFile file) throws IOException {
        StringBuilder builder = new StringBuilder();
        long length = file.length();
        long count = 0;
        for (long seek = length; seek >= 0; --seek) {
            file.seek(seek);

            char c = (char) file.read();
            if (c == '\n') count++;
            if (c != '\n' || seek == 1) {
                builder.append(c);
                //  System.out.println("ouf!!! " + builder.toString());
            } else {
                builder = builder.reverse();
                break;
            }
            if (count < 1)
                count++;
        }
        if (count == 1)
            builder = builder.reverse();
        return builder.toString();

    }
}

