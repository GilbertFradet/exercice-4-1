package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.ResumableImageFileFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingDeserializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseJSONImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageSerializerBase64DatabaseImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.BadgeWalletDAO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges multiples
 */
public class MultiBadgeWalletJSONDAOImpl implements BadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(MultiBadgeWalletJSONDAOImpl.class);

    private final File walletDatabase;
    private final ResumableImageFrameMedia media;

    private ImageStreamingDeserializer<ResumableImageFrameMedia> deserializer;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public MultiBadgeWalletJSONDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
        this.media = new ResumableImageFileFrame(walletDatabase);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     */
    public void addBadge(File image) throws IOException{
        try(ResumableImageFileFrame media = new ResumableImageFileFrame(walletDatabase)){
            ImageStreamingSerializer serializer = new ImageSerializerBase64DatabaseImpl();
            serializer.serialize(image, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     */
    public void getBadge(OutputStream imageStream) throws IOException{
        new ImageDeserializerBase64DatabaseJSONImpl(imageStream).deserialize(this.media);
    }

}
