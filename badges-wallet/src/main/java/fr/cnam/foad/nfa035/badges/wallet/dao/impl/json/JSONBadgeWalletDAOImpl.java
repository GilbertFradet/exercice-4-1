package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseJSONImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletSerializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.JSONWalletDeserializerDAImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Set;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.
 * Version JSON
 */
public class JSONBadgeWalletDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(JSONBadgeWalletDAOImpl.class);

    private final File JSONwalletDatabase;
    private DigitalBadge targetBadge;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public JSONBadgeWalletDAOImpl(String dbPath) throws IOException {
        this.JSONwalletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     *
     * @param image
     * @throws IOException
     * @hidden
     * @deprecated Sur les nouveaux formats de base JSON, Utiliser de préférence addBadge(DigitalBadge badge)
     */
    @Override
    public void addBadge(File image) throws IOException {
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }


    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException {
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(JSONwalletDatabase, "rw"))) {
            ImageStreamingSerializer serializer = new JSONWalletSerializerDAImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     *
     * @param imageStream
     * @throws IOException
     * @deprecated Sur les nouveaux formats de base JSON, Utiliser de préférence getBadgeFromMetadata
     */
   /* @Override
    public void getBadge(OutputStream imageStream) throws IOException{
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(JSONwalletDatabase, "r"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas).deserialize(media);
        }
    }*/
   @Override
   public void getBadge(OutputStream imageStream) throws IOException{
       JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(JSONwalletDatabase, "r"));
       new ImageDeserializerBase64DatabaseJSONImpl(imageStream).deserialize(media);
   }

    /**
     * {@inheritDoc}
     *
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(JSONwalletDatabase, "r"))) {
            return new MetadataDeserializerJSONImpl().deserialize(media);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */

    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try (JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(JSONwalletDatabase, "rw"))) {
            new JSONWalletDeserializerDAImpl(imageStream, metas).deserialize(media, meta);
        }
    }


}
