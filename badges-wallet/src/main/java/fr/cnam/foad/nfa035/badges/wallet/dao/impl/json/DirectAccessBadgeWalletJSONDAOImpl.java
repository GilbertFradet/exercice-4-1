package fr.cnam.foad.nfa035.badges.wallet.dao.impl.json;

import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.WalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.ImageStreamingSerializer;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.ImageDeserializerBase64DatabaseJSONImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.WalletSerializerDirectAccessImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.MetadataDeserializerJSONImpl;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json.WalletDeserializerDirectAccessJSONImpl;
import fr.cnam.foad.nfa035.badges.wallet.dao.DirectAccessBadgeWalletDAO;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.util.Set;

/**
 * DAO simple pour lecture/écriture d'un badge dans un wallet à badges digitaux multiples
 * et PAR ACCES DIRECT, donc prenant en compte les métadonnées de chaque badges.
 */
public class DirectAccessBadgeWalletJSONDAOImpl implements DirectAccessBadgeWalletDAO {

    private static final Logger LOG = LogManager.getLogger(DirectAccessBadgeWalletJSONDAOImpl.class);

    private final File walletDatabase;

    /**
     * Constructeur élémentaire
     *
     * @param dbPath
     * @throws IOException
     */
    public DirectAccessBadgeWalletJSONDAOImpl(String dbPath) throws IOException{
        this.walletDatabase = new File(dbPath);
    }

    /**
     * Permet d'ajouter le badge au Wallet
     * @deprecated Sur les nouveaux formats de base Json, Utiliser de préférence addBadge(DigitalBadge badge)
     * @hidden
     * @param image
     * @throws IOException
     */
    @Override
    public void addBadge(File image) throws IOException{
        LOG.error("Non supporté");
        throw new IOException("Méthode non supportée, utilisez plutôt addBadge(DigitalBadge badge)");
    }

    /**
     * Permet d'ajouter le badge au nouveau format de Wallet
     *
     * @param badge
     * @throws IOException
     */
    @Override
    public void addBadge(DigitalBadge badge) throws IOException{
        try(WalletFrame media = new WalletFrame(new RandomAccessFile(walletDatabase, "rw"))){
            ImageStreamingSerializer serializer = new WalletSerializerDirectAccessImpl(getWalletMetadata());
            serializer.serialize(badge, media);
        }
    }

    /**
     * Permet de récupérer le badge du Wallet
     * @deprecated Sur les nouveaux formats de base CVS, Utiliser de préférence getBadgeFromMetadata
     *
     * @param imageStream
     * @throws IOException
     */
    @Override
    public void getBadge(OutputStream imageStream) throws IOException{
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            new ImageDeserializerBase64DatabaseJSONImpl(imageStream).deserialize(media);
        }
    }


    /**
     * {@inheritDoc}
     * @return List<DigitalBadgeMetadata>
     */
    @Override
    public Set<DigitalBadge> getWalletMetadata() throws IOException {
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "r"))) {
            return new MetadataDeserializerJSONImpl().deserialize(media);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param imageStream
     * @param meta
     * @throws IOException
     */
    @Override
    public void getBadgeFromMetadata(OutputStream imageStream, DigitalBadge meta) throws IOException {
        Set<DigitalBadge> metas = this.getWalletMetadata();
        try(JSONWalletFrame media = new JSONWalletFrame(new RandomAccessFile(walletDatabase, "rw"))) {
            new WalletDeserializerDirectAccessJSONImpl(imageStream, metas).deserialize(media, meta);
        }
    }


}
