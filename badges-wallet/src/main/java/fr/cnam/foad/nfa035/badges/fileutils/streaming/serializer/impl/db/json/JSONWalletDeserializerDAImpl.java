package fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.impl.db.json;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.WalletFrameMedia;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.media.impl.json.JSONWalletFrame;
import fr.cnam.foad.nfa035.badges.fileutils.streaming.serializer.DirectAccessDatabaseDeserializerJSON;
import fr.cnam.foad.nfa035.badges.wallet.model.DigitalBadge;
import org.apache.commons.codec.binary.Base64InputStream;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.*;
import java.util.Set;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public  class JSONWalletDeserializerDAImpl implements DirectAccessDatabaseDeserializerJSON {

    private static final Logger LOG = LogManager.getLogger(JSONWalletDeserializerDAImpl.class);

    private OutputStream sourceOutputStream;
    private Set<DigitalBadge> metas;

    /**
     * Constructeur élémentaire
     *
     * @param sourceOutputStream
     * @param metas              les métadonnées du wallet, si besoin
     */
    public JSONWalletDeserializerDAImpl(OutputStream sourceOutputStream, Set<DigitalBadge> metas) {
        this.setSourceOutputStream(sourceOutputStream);
        this.metas = metas;
    }

    /**
     * {@inheritDoc}
     *
     * @param media
     * @throws IOException
     */

    @Override
    public void deserialize(WalletFrameMedia media, DigitalBadge targetBadge) throws IOException {

        long pos = targetBadge.getMetadata().getWalletPosition();
        media.getChannel().seek(pos);

        JsonFactory jsonFactory = new JsonFactory();
        jsonFactory.configure(JsonGenerator.Feature.AUTO_CLOSE_TARGET,false);
        ObjectMapper objectMapper = new ObjectMapper(jsonFactory);

        String data = media.getEncodedImageReader(false).readLine();
        String badgeData = data.split(",\\{\"payload")[0].split(".*badge\":")[1];

        //
        String badge1 = data.split(",\\{\"payload")[0].split("\\[\\{\"badge\":")[1].split(",\"serial\":")[0];
        DigitalBadge badge = objectMapper.readValue(badgeData,DigitalBadge.class);

        String encodedImageData = data.split(",\\{\"payload\":")[1].split("\"\\}]")[0];
        // Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
        try (OutputStream os = getSourceOutputStream()) {
            getDeserializingStream(encodedImageData).transferTo(os);
        }
        targetBadge.setSerial(badge.getSerial());
        targetBadge.setBegin(badge.getBegin());
        targetBadge.setEnd(badge.getEnd());
    }




    /**
     * {@inheritDoc}
     *
     * @return OutputStream
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * {@inheritDoc}
     *
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        this.sourceOutputStream = os;
    }


/**
 * {@inheritDoc}
 *
 * Inutile => Non pris encharge par cet Objet
 *
 * @param media
 * @throws IOException
 */
  /*  @Override
    public void deserialize(WalletFrameMedia media) throws IOException {
        // Inutile
        throw new IOException("Non pris encharge par cet Objet");
    }*/

    /**
     * {@inheritDoc}
     *
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes()));
    }

    @Override
    public void deserialize(JSONWalletFrame media) throws IOException {

    }
}
