package fr.cnam.foad.nfa035.badges.gui.view;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class TestObjectMapper {
    public ObjectMapper objectMapper;
    private static final String RESOURCES_PATH = "badges-gui/src/main/resources/";
    private File file = new File(RESOURCES_PATH + "car.json");
    private FileWriter fw;

    public TestObjectMapper() throws IOException {
    }

    public void map(String color, String type) throws IOException {
        this.objectMapper = new ObjectMapper();
        fw = new FileWriter(file, true);
        Car car = new Car(color, type);
        String[] tab = {"One", "two", "three"};
        objectMapper.writeValue(fw, tab );
        fw.close();
        FileWriter  fw2 = new FileWriter(file, true);
        fw2.append(',');
        fw2.close();

    }

    public static void main(String[] args) throws IOException {
        TestObjectMapper testObjectMapper = new TestObjectMapper();

        testObjectMapper.map("yellow", "renault");
        testObjectMapper.map("yellow", "renault");
    }

}

class Car {
    private String color;
    private String type;

    public void setColor(String color) {
        this.color = color;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Car(String color, String type) {
        this.color = color;
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public String getType() {
        return type;
    }
}
